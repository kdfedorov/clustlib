#include <iostream>
#include <clustlib.h>

void print_object(const clustlib::object& obj, int level = 0) {
   for (auto i = 0; i < level; ++i) std::cout << '\t';
   std::cout << "object: {";
   for (const auto& val : obj.data) {
      std::cout << val << " ";
   }
   std::cout << "}\n";
}

void print_cluster(const clustlib::cluster& cl, int level = 0) {
   for (auto i = 0; i < level; ++i) std::cout << '\t';
   std::cout << "cluster: \n";
   for (auto i = 0; i < cl.size(); ++i)
      print_object(cl[i], level);
}

void print_tree(const clustlib::cluster_tree& ct, int level) {
   for (auto i = 0; i < level; ++i) std::cout << '\t';
   print_cluster(ct.parent, level);
   for (const auto& child : ct.children) {
      print_tree(child, level + 1);
   }
}

#include <fstream>
int main() {
   using namespace clustlib;
   csv file(clustlib::dataset({}));
   file.insert_row();
   file.insert_column();
   file.set(0, 0, std::string("s"));
   csv::table_content tc;
   file.content(tc);
   puts(tc[0][0].c_str());
   return 0;
}
