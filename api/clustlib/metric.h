#pragma once
/* FILE: clustlib/metric.h
   DESC: This file contains clustlib metrics. */
#include "cluster.h"

namespace clustlib {
   // Metric function signature
   using metric_f = clustlib::value_t(*)(const clustlib::object&, const clustlib::object&);

   ////////////////////////////////////////
   // Metrics
   CLUSTLIB_API clustlib::value_t euclidean(const clustlib::object& x, const clustlib::object& y) noexcept(false);
   CLUSTLIB_API clustlib::value_t euclidean_sq(const clustlib::object& x, const clustlib::object& y) noexcept(false);
   CLUSTLIB_API clustlib::value_t manhattan(const clustlib::object& x, const clustlib::object& y) noexcept(false);
   CLUSTLIB_API clustlib::value_t chebyshev(const clustlib::object& x, const clustlib::object& y) noexcept(false);

   // Power distance native function (signature incompatible, but uses real numbers)
   CLUSTLIB_API clustlib::value_t fpower_dist(const clustlib::object& x, const clustlib::object& y,
      clustlib::value_t root = 2.0, clustlib::value_t power = 2.0) noexcept(false);

   // Signature-compatible power distance function
   template <clustlib::number_t root, clustlib::number_t power>
   CLUSTLIB_API clustlib::value_t power_dist(const clustlib::object& x, const clustlib::object& y) noexcept(false) {
      return fpower_dist(x, y, value_t(root), value_t(power));
   }
   
   ////////////////////////////////////////
}