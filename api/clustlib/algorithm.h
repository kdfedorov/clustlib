#pragma once
/* FILE: clustlib/algorithm.h
   DESC: This file contains clustlib cluster analysis algorithms. */

#include <map>

#include "cluster.h"
#include "metric.h"

namespace clustlib {
   // List of implemented types of clustering algorithms
   enum class algtypes {
      type_centroid_based,    // Centroid-based algorithm type (Partitioning method)
      type_connectivity_based // Connectivity-based algorithm type (Hiearchical clustering)
   };

   // List of implemented algorithms
   enum class algorithms {
      alg_kohonen,               // Unsupervised Kohonen's neural network
      alg_kohonen_so,            // Self-organized Kohonen's neural network
      alg_hierarchical_kohonen   // Hierarchical Kohonen's neural network
   };

   ///////////////////////////////////////////////////////////////////////
   // Abstract clustering algorithm
   class CLUSTLIB_API algorithm {
   public:
      // Loads new dataset to algorithm
      virtual void load_dataset(const dataset* ds) noexcept(false);

      // Sets a metric function to algorithm
      void load_metric(metric_f dist);

      // Execute clusterization
      virtual bool clusterize() = 0;

      // Clusterization quick call
      inline bool operator()() { return clusterize(); }

      // Returns type of algorithm
      virtual algtypes type() const = 0;

      // Returns algorithm identification
      virtual algorithms alg() const = 0;

   protected:
      // Access to current dataset
      const dataset* get_dataset() const;
      // Returns count of attributes in current dataset
      std::size_t attribute_count() const;
      // Returns count of samples in current dataset
      std::size_t samples_count() const;
      // Calculates distance between objects
      clustlib::value_t distance(const clustlib::object& x,
         const clustlib::object& y) const noexcept(false);
      // Returns selected metric
      metric_f metric() const;

   private:
      // Current dataset
      const clustlib::dataset* __ds;
      // Algorithms metric
      metric_f __dist;

      std::size_t __attrcount;   // Current dataset's attribute count
      std::size_t __samplcount;  // Current dataset's samples count
   };

   ///////////////////////////////////////////////////////////////////////
   // Abstract centroid-based algorithm
   class CLUSTLIB_API centroid_based : public virtual algorithm {
   public:
      // Returns that derived algorithms are centroid-based
      inline algtypes type() const override final { return algtypes::type_centroid_based; }

      // Returns result of centroid-based clustering
      virtual void result(cluster_vec& clusters) const;

      // Sets target cluster count
      void set_cluster_count(std::size_t cluster_count);
   protected:
      // Bridge structure lets us attach object in dataset and cluster positions
      using bridge = std::map <std::size_t, std::size_t>;

      // Moves object with ds_idx index in dataset to cluster #cl_idx.
      void move(std::size_t ds_idx, std::size_t cl_idx);

      // Read-only access to current clusters
      const cluster_vec& clusters() const;

      // Returns target count of clusters
      std::size_t clusters_count() const;

      // Removes empty clusters
      void remove_empty();

      // Adds cluster
      std::size_t add_cluster();

   private:
      std::size_t __clcount;  // Current clusters count
      bridge      __bridge;   // Bridge between clusters and dataset
      cluster_vec __cl;       // Clusters
   };

   ///////////////////////////////////////////////////////////////////////
   // Abstract connectivity-based algorithm
   class CLUSTLIB_API connectivity_based : public virtual algorithm {
   public:
      // Cluster tree structure
      struct cluster_tree {
         cluster parent;
         std::vector <cluster_tree> children;
      };

      // Returns that derived algorithms are connectivity-based
      inline algtypes type() const override final { return algtypes::type_connectivity_based; }
   
      // Returns result of connectivity-based clustering
      void result(cluster_tree& clusters) const;

   protected:
      cluster_tree __cl;
   };

   // Straight-ahead refering
   using cluster_tree = connectivity_based::cluster_tree;

   ///////////////////////////////////////////////////////////////////////
   // Neural network weights definition
   using weights = std::vector <valvec>;
   
   // Abstract clustering neural network
   class CLUSTLIB_API neural_network : public virtual centroid_based {
   public:
      // Common clustering neural network parameters
      struct settings {
         settings() = default;
         // Count of eras
         clustlib::number_t eras;
         // Learning rate
         clustlib::value_t learning_rate;
         // Learning rate downturn
         clustlib::value_t rate_downturn;
         // Minimal allowed evolution value
         clustlib::value_t threshold;
      };

      // Execute clusterization
      bool clusterize() override final;

      // Load new neural network settings
      void load_settings(const neural_network::settings& params);

      // Loads new dataset to algorithm
      virtual void load_dataset(const dataset* ds) noexcept(false) override;

      // Toggles dataset normalization
      void set_normalization(bool b);

      // Returns result of centroid-based clustering
      virtual void result(cluster_vec& clusters) const override;

   protected:
      // Returns current training parameters
      const settings& get_settings() const;

      // Returns neural network weights
      weights& get_weights();
      // Returns read-only neural network weights
      const weights& get_weights() const;

      // Checks, if all needed parameters were configured
      virtual bool is_ready() const;
      // Initializes weights
      virtual void init_weights() = 0;
      // Returns closest cluster to given sample
      std::size_t mindist_cluster(const object& obj) const;

      // Network's main loop
      virtual bool loop(value_t rate) = 0;

   private:
      // Neural network training parameters
      settings __s;
      // Neural network weights
      weights __w;
      // Normalized dataset
      dataset __dsnorm;
      // Original dataset
      const dataset* __dsorig;

      bool __norm;
   };

   ///////////////////////////////////////////////////////////////////////
   // Kohonen's unsupervised neural network
   class CLUSTLIB_API kohonen : public virtual neural_network {
   public:
      // Shortcut
      using settings = neural_network::settings;

      // Default constructor
      kohonen(const dataset* ds = nullptr, bool norm = true, const settings& params = settings(),
         std::size_t cluster_count = 0, metric_f dist = &euclidean) noexcept(false);

      // Returns that this is a Kohonen's neural network
      inline algorithms alg() const override final { return algorithms::alg_kohonen; }
   
   private:
      // Network's main loop
      bool loop(value_t rate) override;
      // Initializes weights
      virtual void init_weights() override;
   };

   ///////////////////////////////////////////////////////////////////////
   // Kohonen's self-ordered neural network
   class CLUSTLIB_API kohonen_so : public virtual neural_network {
   public:
      // Default constructor
      kohonen_so(const dataset* ds = nullptr, bool norm = true, const settings& params = settings(),
        value_t crit_d = nullval, metric_f dist = &euclidean) noexcept(false);

      // Returns that this is a Kohonen's neural network
      inline algorithms alg() const override final { return algorithms::alg_kohonen_so; }

      // Sets new critical distance
      void set_crit_distance(value_t dist);
   private:
      // Checks, if all needed parameters were configured
      virtual bool is_ready() const override;
      // Initializes weights
      virtual void init_weights() override;
      // Network's main loop
      bool loop(value_t rate) override;

   private:
      // Maximum allowed distance to existing clusters
      clustlib::value_t __crit;
   };

   ///////////////////////////////////////////////////////////////////////
   // Hierarchical Kohonen (Recurse self-organized)
   class CLUSTLIB_API hierarchical_kohonen : public virtual connectivity_based {
   public:
      // Shortcut
      using settings = neural_network::settings;
      
      // Default constructor
      hierarchical_kohonen(const dataset* ds = nullptr, const settings& params = settings(),
         value_t crit_d = nullval, metric_f dist = &euclidean) noexcept(false);

      // Returns algorithm identification
      inline algorithms alg() const override final { return algorithms::alg_hierarchical_kohonen; }

      // Checks, if all needed parameters were configured
      bool is_ready() const;

      // Execute clusterization
      bool clusterize() override;

      // Load new neural network settings
      void load_settings(const neural_network::settings& params);

      // Sets new critical distance
      void set_crit_distance(value_t dist);

   protected:
      // Returns current training parameters
      const settings& get_settings() const;

   private:
      settings __s;
      value_t  __crit;
   };
   ///////////////////////////////////////////////////////////////////////
}
