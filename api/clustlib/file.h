#pragma once
/* FILE: clustlib/file.h
   DESC: This file implements supported file formats writing/reading. */

#include <list>
#include <string>
#include <fstream>

#include "cluster.h"

namespace clustlib {
   // .csv format reader/writer
   class CLUSTLIB_API csv {
   public:
      //// Public table content structures ////////////////////////////////
      using line_content = std::vector <std::string>;    // Content of the line
      using table_content = std::vector <line_content>;  // Content of the table

      //// Quick read/write ///////////////////////////////////////////////
      /* csv class allows you to manipulate opened .csv file. But interacting
         with file in such way lets it's content to place in RAM. It's 
         unneccesary if you want to just read or write file, so that's
         what for this "quick" read and write functions needed. */
      /* WARNING: Using this causes lost of string attributes! (they turn into null) */

      // Reads dataset from .csv file
      static bool read(const char* filename, clustlib::dataset& ds);
      // Writes dataset to .csv file
      static bool write(const char* filename, const clustlib::dataset& ds);
      /////////////////////////////////////////////////////////////////////

      // Fresh file constructor
      csv(const clustlib::dataset& ds = {});
      // Open file constructor (if file doesn't exist it will be fresh by default
      csv(const char* filename);
      
      // Opens .csv file
      bool open(const char* filename);
      // Closes current file
      void close();

      // Returns current number of lines
      clustlib::number_t linecount() const;
      // Returns current number of attributes
      clustlib::number_t attrcount() const;
      // Returns table content
      void content(table_content& table) const;
      
      // Reads opened file
      bool read(clustlib::dataset& ds) const;
      // Reads object from opened file
      bool readline(clustlib::number_t line, clustlib::object& obj) const noexcept(false);
      
      // Writes current stored data to file
      bool write(const char* filename) const;

      // Returns attribute value at cell
      const char* get(number_t row, number_t col) const noexcept(false);
      // Sets attribute value to cell
      void set(number_t row, number_t col, std::string& val) noexcept(false);
      
      // Cuts file content by given line
      void cutline(clustlib::number_t line) noexcept(false);
      // Cuts file content by given range
      void cutline(clustlib::number_t lstart, clustlib::number_t lend) noexcept(false);
      
      // Cuts file column by given index
      void cutcolumn(clustlib::number_t column) noexcept(false);
      // Cuts file columns by given range
      void cutcolumn(clustlib::number_t cstart, clustlib::number_t cend) noexcept(false);
      
      // Inserts object to file before given line (beyond last, by default)
      void insert(const clustlib::object& obj, clustlib::number_t line = -1) noexcept(false);
      // Inserts set of objects to file before given line (beyond last, by default)
      void insert(const clustlib::dataset& ds, clustlib::number_t line = -1) noexcept(false);

      // Empty inserts
      // Inserts empty row to file before given line (beyond last, by default)
      void insert_row(clustlib::number_t line = -1) noexcept(false);
      // Inserts empty column to file before given column (beyond last, by default)
      void insert_column(clustlib::number_t column = -1) noexcept(false);
      
   private:
      // Data structure used to store parsed .csv row
      using valdata = std::list <std::string>;
      // Data structure used to store parsed .csv file
      using csvdata = std::list <valdata>;

      // Iterators ///////////////////////
      //// Constant ////
      using iter_cf  = csvdata::const_iterator;
      using iter_cr  = csvdata::const_reverse_iterator;
      //// Regular ////
      using iter_mf  = csvdata::iterator;
      using iter_mr  = csvdata::reverse_iterator;
      ////////////////////////////////////

      // Prepares object data (returns attribute count)
      std::size_t prepare(const clustlib::object& obj, valdata& vdat) const;
      // Prepares dataset (returns attribute count)
      std::size_t prepare(const clustlib::dataset& ds, csvdata& csvd) const noexcept(false);

      // Parses line of file
      bool parse(std::ifstream& rfile, valdata& vdat) const;

      // Returns selected line
      iter_cf getline(clustlib::number_t line) const noexcept(false);
      iter_mf getline(clustlib::number_t line) noexcept(false);
      // Returns selected attribute
      valdata::const_iterator getattr(const valdata& vdat, clustlib::number_t i) const noexcept(false);

      // Transforms parsed data back to text line
      void put_together(const valdata& vdat, std::string& line) const;

   private:
      // Opened file name
      mutable std::string __filename;

      std::size_t __attrcount;   // File data set attributes count
      csvdata     __data;        // Parsed file data
   };
}
