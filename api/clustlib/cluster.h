#pragma once
/* FILE: clustlib/cluster.h
   DESC: This file contains all clustlib basic types, structures and constants,
         including cluster. */

#ifdef _WIN32
#define CLUSTLIB_API __declspec(dllexport)
#else
#define CLUSTLIB_API
#endif

#include <cstdint>
#include <vector>
#include <stdexcept>
#include <functional>
#include <limits>

///////////////////////////////////////////////////////////////////////
namespace clustlib {
   ////////////////////////////////////////////////////////////////////
   // Types
   
   // Type used for attribute value
   using value_t = double;

   // Type used for integer values
   using number_t = int32_t;

   // Most often reason to use vector is contain attributes
   using valvec = std::vector <value_t>;

   ////////////////////////////////////////////////////////////////////
   // Constants
   constexpr value_t    nullval  = 0.0;
   constexpr value_t    minval   = std::numeric_limits<value_t>::min();
   constexpr value_t    maxval   = std::numeric_limits<value_t>::max();

   constexpr number_t   nullnum  = 0;
   constexpr number_t   minnum   = std::numeric_limits<number_t>::min();
   constexpr number_t   maxnum   = std::numeric_limits<number_t>::max();
   ////////////////////////////////////////////////////////////////////
   // Classes
   
   // Object class
   struct object {
      // Attributes container
      valvec data;

      // Constructors //////////////////////////////
      //// Default /////////////////////////////////
      object() = default;
      //// with Parameters /////////////////////////
      object(valvec _data) : data(_data) {}
      object(std::size_t size) {
         data.resize(size);
         std::fill(data.begin(), data.end(), nullval);
      }

      // Shortcuts /////////////////////////////////
      inline void             clear()                          { data.clear(); }
      inline void             resize(std::size_t i)            { data.resize(i); }
      inline value_t&         operator[](std::size_t i)        { return data[i]; }
      inline std::size_t      size()                     const { return data.size(); }
      inline bool             empty()                    const { return data.empty(); }
      inline const value_t&   operator[](std::size_t i)  const { return data[i]; }

      // Methods ///////////////////////////////////
      
      // Returns current object size, if they are same size, else - maxval
      bool match(const object& obj) const noexcept(false) {
         bool res = true;
         if (size() != obj.size()) {
            throw std::logic_error("Object sizes are mismatching!");
            res = false;
         }

         return res;
      }

      template <class rel_> // Abstract object comparing
      bool compare(const object& obj, rel_ cmp) const noexcept(false) {
         std::size_t size = this->size();
         if (match(obj)) {
            for (auto i = 0; i < size; ++i)
               if (!cmp(data[i], obj[i]))
                  return false;

            return true;
         }

         return false;
      }

      template <class op_> // Abstract object arithmetics
      object operation(const object& obj, op_ op) const noexcept(false) {
         std::size_t size = this->size();
         object res(size);
         
         if (match(obj)) {
            for (auto i = 0; i < size; ++i)
               res[i] = op(data[i], obj[i]);
         }
         
         return res;
      }

      template <class op_> // Abstract this object transformation
      object transformation(const object& obj, op_ op) noexcept(false) {
         std::size_t size = this->size();
         if (match(obj)) {
            for (auto i = 0; i < size; ++i)
               data[i] = op(data[i], obj[i]);
         }

         return *this;
      }

      template <class fn_>
      void foreach(fn_ fn) {
         for (auto& val : data)
            fn(val);
      }

      template <class op_>
      value_t accumulate(op_ op) const {
         value_t res = nullval;
         for (const auto& val : data)
            res = op(res, val);
         return res;
      }

      // Operators /////////////////////////////////
      //// Equality ////////////////////////////////
      inline bool operator==(const object& obj) const {
         if (this == &obj)
            return true;
         return compare(obj, std::equal_to());
      }

      inline bool operator!=(const object& obj) const {
         if (this == &obj)
            return false;
         return !compare(obj, std::equal_to());
      }

      //// Artithmetics ////////////////////////////
      inline object operator+(const object& obj) const {
         return operation(obj, std::plus());
      }

      inline object operator+=(const object& obj) {
         return transformation(obj, std::plus());
      }

      inline object operator-(const object& obj) const {
         return operation(obj, std::minus());
      }

      inline object operator-=(const object& obj) {
         return transformation(obj, std::minus());
      }

      inline object operator*(const object& obj) const {
         return operation(obj, std::multiplies());
      }

      inline object operator*=(const object& obj) {
         return transformation(obj, std::multiplies());
      }

      inline object operator/(const object& obj) const {
         return operation(obj, std::divides());
      }

      inline object operator/=(const object& obj) {
         return transformation(obj, std::divides());
      }

      //////////////////////////////////////////////
   };

   // Dataset definition
   using dataset = std::vector <object>;

   // Cluster class
   class CLUSTLIB_API cluster {
   public:
      cluster(const dataset& ds = {}) noexcept(false);

      // Adds object to cluster
      void add(const object& obj) noexcept(false);

      // Removes object from cluster
      void remove(const object& obj);
      
      // Removes object from cluster by index
      void remove(std::size_t i) noexcept(false);

      // Moves object to another cluster by index
      void move(std::size_t i, cluster& target) noexcept(false);

      // Moves object to another cluster
      bool move(const object& obj, cluster& target);

      // Clears cluster
      void clear();

      // Returns attribute count of cluster objects
      std::size_t attribute_count() const; 

      // Returns size of cluster
      std::size_t size() const; 

      // Returns true, if cluster is empty
      bool empty() const; 

      // Returns chosen cluster element
      const object& operator[](std::size_t i) const;

      // Returns cluster's centroid
      void centroid(clustlib::object& c) const;

      // Returns read-only access to dataset of cluster
      const dataset& get_dataset() const;

      // Returns full access to dataset of cluster
      dataset& get_dataset();
   private:
      dataset     __objects;     // Cluster content
      std::size_t __attrcount;   // Attributes in object count
   };

   // Pseudonym for set of clusters
   using cluster_vec = std::vector <clustlib::cluster>;

   ////////////////////////////////////////////////////////////////////
}
///////////////////////////////////////////////////////////////////////
