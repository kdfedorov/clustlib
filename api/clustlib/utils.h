#pragma once
/* FILE: clustlib/utils.h
   DESC: This file contains clustlib useful functions. */
#include "cluster.h"

/////////////////////////////////////////////////////////
namespace clustlib {
   // Matches objects attribute count in dataset
   CLUSTLIB_API bool is_integral_dataset(const clustlib::dataset& ds);
   // Calculates dataset center of mass
   CLUSTLIB_API void center_of_mass(const clustlib::dataset& ds, clustlib::object& centroid) noexcept(false);
   // Normalizes number value
   CLUSTLIB_API void normalize_val(value_t& val);
   // Normalizes dataset
   CLUSTLIB_API void normalize_ds(dataset& ds);
}
/////////////////////////////////////////////////////////
