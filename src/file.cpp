#include <iterator>
#include "clustlib/file.h"
#include "clustlib/utils.h"

namespace clustlib {
   // Default file interactions modes
   constexpr auto rflag = std::ios::in;
   constexpr auto wflag = std::ios::out | std::ios::trunc;

   // Useful functions
   namespace utils {
      template <class t_>
      typename std::list <t_>::const_iterator list_index_search(const std::list <t_>& lst,
         clustlib::number_t i) {
         std::size_t size = lst.size();
         if (i < 0)
            i = size - i;
         else if (i >= size) {
            throw std::out_of_range("Referencing a non-existing element.");
            return lst.cend();
         }

         if (i < size / 2) {
            auto it = lst.cbegin();
            auto end = lst.cend();
            while (i > 0) {
               it++;
               i -= 1;
            }

            return it;
         }
         else {
            i = size - 1 - i;
            auto it = lst.crbegin();
            auto end = lst.crend();
            while (i > 0) {
               it++;
               i -= 1;
            }

            it++;
            return it.base();
         }
      }

      template <class t_>
      typename std::list <t_>::iterator list_index_search_mut(std::list <t_>& lst,
         clustlib::number_t i) {
         std::size_t size = lst.size();
         if (i < 0)
            i = size - i;
         else if (i >= size) {
            throw std::out_of_range("Referencing a non-existing element.");
            return lst.end();
         }

         if (i < size / 2) {
            auto it = lst.begin();
            auto end = lst.end();
            while (i > 0) {
               it++;
               i -= 1;
            }

            return it;
         }
         else {
            i = size - 1 - i;
            auto it = lst.rbegin();
            auto end = lst.rend();
            while (i > 0) {
               it++;
               i -= 1;
            }

            it++;
            return it.base();
         }
      }
   }

   bool csv::read(const char* filename, clustlib::dataset& ds) {
      ds.clear();
      std::ifstream rfile(filename, rflag);
      if (!rfile.is_open())
         return false;

      while (!rfile.eof()) {
         std::string linestr = "";
         std::string strval = "";
         bool        quote = false;
         object      obj;

         std::getline(rfile, linestr);

         if (!linestr.empty()) {
            for (char c : linestr) {
               switch (c) {
                  case '\"': {
                     quote = !quote;
                     break;
                  }
                  case ',': {
                     if (quote)
                        strval += c;
                     else {
                        obj.data.push_back(std::atof(strval.c_str()));
                        strval.clear();
                     }

                     break;
                  }
                  case ' ': {
                     if (quote)
                        strval += c;
                     break;
                  }
                  default: {
                     strval += c;
                     break;
                  }
               }
            }

            if (!strval.empty()) {
               obj.data.push_back(std::atof(strval.c_str()));

               try {
                  if (!ds.empty())
                     obj.match(*ds.cbegin());
               }
               catch (const std::logic_error&) {
                  ds.clear();
                  rfile.close();
                  return false;
               }

               ds.push_back(obj);
            }
         }
      }

      rfile.close();
      return true;
   }

   bool csv::write(const char* filename, const clustlib::dataset& ds) {
      if (!is_integral_dataset(ds))
         return false;
      
      std::ofstream wfile(filename, wflag);
      if (!wfile.is_open())
         return false;

      if (!ds.empty()) {
         auto size = ds.cbegin()->size();
         for (const auto& obj : ds) {
            for (auto i = 0; i < size; ++i) {
               wfile << std::to_string(obj[i]);
               if (i != (size - 1))
                  wfile << ',';
            }
            wfile << '\n';
         }
      }

      wfile.close();
      return true;
   }

   csv::csv(const clustlib::dataset& ds/*= {}*/) : __filename("") {
      __data.clear();
      __attrcount = prepare(ds, __data);
   }

   csv::csv(const char* filename) {
      if (!filename)
         filename = "";
      __data.clear();
      __attrcount = 0;
      open(filename);
   }

   bool csv::open(const char* filename) {
      if (!__filename.empty())
         close();

      std::ifstream rfile(filename, rflag);
      if (!rfile.is_open()) {
         return false;
      }

      __filename = filename;
      while (!rfile.eof()) {
         valdata vdat;
         if (!parse(rfile, vdat))
            continue;
         
         if (__data.empty())
            __attrcount = vdat.size();
         else if (vdat.size() != __attrcount) {
            rfile.close();
            this->close();
            return false;
         }
         
         __data.push_back(vdat);
      }

      rfile.close();
      return true;
   }

   void csv::close() {
      __filename = "";
      __attrcount = 0;
      __data.clear();
   }

   clustlib::number_t csv::linecount() const {
      return __data.size();
   }

   clustlib::number_t csv::attrcount() const {
      if (linecount() == 0)
         return 0;
      return __data.cbegin()->size();
   }

   void csv::content(table_content& table) const {
      table.clear();
      for (const auto& line : __data) {
         line_content lc;
         for (const auto& val : line) {
            lc.push_back(val);
         }
         table.push_back(lc);
      }
   }

   bool csv::read(clustlib::dataset& ds) const {
      bool res = true;
      ds.clear();
      auto size = linecount();
      
      for (auto i = 0; i < size; ++i) {
         object obj;
         if (!readline(i, obj)) {
            res = false;
            ds.clear();
            break;
         }

         ds.push_back(obj);
      }

      return res;
   }

   bool csv::readline(clustlib::number_t line, clustlib::object& obj) const {
      obj.clear();
      auto iter = getline(line);
      if (iter == __data.cend())
         return false;

      const valdata& vdat = *iter;
      for (const auto& strval : vdat)
         obj.data.push_back(std::atof(strval.c_str()));

      return true;
   }

   bool csv::write(const char* filename) const {
      std::ofstream wfile(filename, wflag);
      if (!wfile.is_open()) {
         return false;
      }

      if (__filename.empty())
         __filename = filename;

      for (const auto& vdat : __data) {
         std::string line = "";
         put_together(vdat, line);
         wfile << line << std::endl;
      }

      wfile.close();
      return true;
   }

   const char* csv::get(number_t row, number_t col) const noexcept(false) {
      if (row > linecount() || row < 0) {
         throw std::logic_error("Given line doesn't exists.");
         return nullptr;
      }

      if (col > attrcount() || col < 0) {
         throw std::logic_error("Given column doesn't exists.");
         return nullptr;
      }

      auto it_row = getline(row);
      auto it_val = std::next(it_row->begin(), row);
      return it_val->c_str();
   }

   void csv::set(number_t row, number_t col, std::string& val) noexcept(false) {
      if (row > linecount() || row < 0) {
         throw std::logic_error("Given line doesn't exists.");
         return;
      }

      if (col > attrcount() || col < 0) {
         throw std::logic_error("Given column doesn't exists.");
         return;
      }

      auto it_row = getline(row);
      auto it_val = std::next(it_row->begin(), row);
      *it_val = val;
   }

   void csv::cutline(clustlib::number_t line) noexcept(false) {
      auto iter = getline(line);
      if (iter != __data.cend())
         __data.erase(iter);
   }

   void csv::cutline(clustlib::number_t lstart, clustlib::number_t lend) noexcept(false) {
      auto it_start = getline(lstart);
      auto it_end = getline(lend);
      __data.erase(it_start, it_end);
   }

   void csv::cutcolumn(clustlib::number_t column) noexcept(false) {
      for (auto& vdat : __data) {
         auto iter = getattr(vdat, column);
         vdat.erase(iter);
      }
      __attrcount--;
   }

   void csv::cutcolumn(clustlib::number_t cstart, clustlib::number_t cend) noexcept(false) {
      for (auto& vdat : __data) {
         auto it_start = getattr(vdat, cstart);
         auto it_end = getattr(vdat, cend);
         vdat.erase(it_start, it_end);
      }
      __attrcount -= std::abs(cend - cstart);
   }

   void csv::insert(const clustlib::object& obj, clustlib::number_t line/*= -1*/) noexcept(false) {
      valdata vdat;
      if (prepare(obj, vdat) != __attrcount) {
         throw std::logic_error("Attribute counts of inserting object and file dataset doesn't match.");
         return;
      }

      if (line > linecount()) {
         throw std::logic_error("Given line doesn't exists.");
         return;
      }

      auto it = (line == -1) ? __data.cend() : getline(line);
      __data.insert(it, vdat);
   }

   void csv::insert(const clustlib::dataset& ds, clustlib::number_t line/*= -1*/) noexcept(false) {
      csvdata csvd;
      if (prepare(ds, csvd) != __attrcount) {
         throw std::logic_error("Attribute counts of inserting dataset and file dataset doesn't match.");
         return;
      }

      if (line > linecount()) {
         throw std::logic_error("Given line doesn't exists.");
         return;
      }

      auto it = (line == -1) ? __data.cend() : getline(line);

      for (const auto& vdat : csvd)
         __data.insert(it, vdat);
   }

   void csv::insert_row(clustlib::number_t line/*= -1*/) noexcept(false) {
      if (line > linecount()) {
         throw std::logic_error("Given line doesn't exists.");
         return;
      }
      
      auto it = (line == -1) ? __data.cend() : getline(line);

      valdata vdat;
      vdat.resize(attrcount());
      std::fill(vdat.begin(), vdat.end(), "");

      __data.insert(it, vdat);
   }

   void csv::insert_column(clustlib::number_t column/*= -1*/) noexcept(false) {
      if (column > attrcount()) {
         throw std::logic_error("Given column doesn't exists.");
         return;
      }

      column = (column == -1) ? attrcount() : column;
      for (auto& vdat : __data) {
         auto it = std::next(vdat.begin(), column);
         vdat.insert(it, "");
      }
   }

   std::size_t csv::prepare(const clustlib::object& obj, csv::valdata& vdat) const {
      vdat.clear();
      for (auto val : obj.data)
         vdat.push_back(std::to_string(val));
      return obj.size();
   }

   std::size_t csv::prepare(const clustlib::dataset& ds, csvdata& csvd) const noexcept(false)  {
      csvd.clear();
      if (ds.empty())
         return 0;

      if (!is_integral_dataset(ds)) {
         throw std::logic_error("Given dataset is not integral. Objects attribute counts doesn't match.");
         return 0;
      }

      for (const auto& obj : ds) {
         valdata vdat;
         prepare(obj, vdat);
         csvd.push_back(vdat);
      }

      return ds.cbegin()->size();
   }

   bool csv::parse(std::ifstream& rfile, valdata& vdat) const {
      vdat.clear();
      std::string line = "";

      std::getline(rfile, line);
      if (line.empty())
         return false;

      std::string strval = "";
      bool quote = false;
      for (char c : line) switch (c) {
         case '\"': {
            quote = !quote;
            break;
         }

         case ',': {
            if (quote)
               strval += c;
            else {
               vdat.push_back(strval);
               strval.clear();
            }

            break;
         }

         default: {
            strval += c;
            break;
         }
      }

      if (!strval.empty())
         vdat.push_back(strval);

      return true;
   }

   csv::iter_cf csv::getline(clustlib::number_t line) const noexcept(false) {
      auto res = __data.cend();

      try {
         res = utils::list_index_search(__data, line);
      }
      catch (const std::logic_error&) {
         throw std::out_of_range("Referencing a non-existing line.");
      }

      return res;
   }

   csv::iter_mf csv::getline(clustlib::number_t line) noexcept(false) {
      auto res = __data.end();

      try {
         res = utils::list_index_search_mut(__data, line);
      }
      catch (const std::logic_error&) {
         throw std::out_of_range("Referencing a non-existing line.");
      }

      return res;
   }

   csv::valdata::const_iterator csv::getattr(
      const csv::valdata& vdat, clustlib::number_t i) const noexcept(false) {
      auto res = vdat.cend();

      try {
         res = utils::list_index_search(vdat, i);
      }
      catch (const std::logic_error&) {
         throw std::out_of_range("Referencing a non-existing attribute.");
      }

      return res;
   }

   void csv::put_together(const valdata& vdat, std::string& line) const {
      line = "";
      for (auto it = vdat.cbegin(); it != vdat.cend(); it = std::next(it)) {
         line += *it;
         if (it != getattr(vdat, -1))
            line += ',';
      }
   }
}
