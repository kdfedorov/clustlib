#include "clustlib/cluster.h"
#include "clustlib/utils.h"

namespace clustlib {
   cluster::cluster(const dataset& ds/*= {}*/) noexcept(false) {
      __objects.clear();
      __attrcount = ds.empty() ? 0 : ds.cbegin()->size();
      for (const auto& obj : ds)
         add(obj);
   }

   void cluster::add(const object& obj) noexcept(false) {
      if (empty())
         __attrcount = obj.size();

      if (obj.size() != attribute_count())
         throw std::logic_error("Object size doesn't match cluster objects size!");
      else
         __objects.push_back(obj);
   }

   void cluster::remove(const object& obj) {
      if (obj.size() == attribute_count()) {
         __objects.erase(std::remove(__objects.begin(), __objects.end(), obj));
      }

      if (size() == 0)
         __attrcount = 0;
   }

   void cluster::remove(std::size_t i) noexcept(false) {
      if (i >= size())
         throw std::out_of_range("Bad cluster object index.");
      else
         __objects.erase(std::remove(__objects.begin(), __objects.end(), __objects[i]));
   }

   void cluster::move(std::size_t i, cluster& target) noexcept(false) {
      if (i >= size())
         throw std::out_of_range("Bad cluster object index.");
      else {
         target.add(__objects[i]);
         auto it = std::remove(__objects.begin(), __objects.end(), __objects[i]);
         __objects.erase(it);
      }
   }

   bool cluster::move(const object& obj, cluster& target) {
      auto it = std::find(__objects.cbegin(), __objects.cend(), obj);
      if (it == __objects.cend())
         return false;

      target.add(obj);
      __objects.erase(it);

      return true;
   }

   void cluster::clear() {
      __objects.clear();
      __attrcount = 0;
   }

   std::size_t    cluster::attribute_count()          const { return __attrcount; }
   std::size_t    cluster::size()                     const { return __objects.size(); }
   bool           cluster::empty()                    const { return __objects.empty(); }
   const object&  cluster::operator[](std::size_t i)  const { return __objects[i]; }

   void cluster::centroid(clustlib::object& c) const {
      center_of_mass(__objects, c);
   }

   const dataset& cluster::get_dataset() const {
      return __objects;
   }

   dataset& cluster::get_dataset() {
      return __objects;
   }
}
