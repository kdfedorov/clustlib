#include <algorithm>
#include "clustlib/utils.h"

namespace clustlib {
   bool is_integral_dataset(const clustlib::dataset& ds) {
      if (ds.empty())
         return true;

      auto attrcount = ds.cbegin()->size();
      for (auto obj : ds)
         if (obj.size() != attrcount)
            return false;

      return true;
   }

   void center_of_mass(const clustlib::dataset& ds, clustlib::object& centroid) noexcept(false) {

      centroid.clear();

      if (!ds.empty()) {
          auto size = ds.cbegin()->size();
          centroid.resize(size);
          centroid.foreach([](value_t& val) { val = nullval; });

          for (const auto& obj : ds)
            centroid += obj;

          size = ds.size();
          centroid.foreach([size](value_t& val) { val /= size;});
      }
   }

   void normalize_val(value_t& val) {
      while (std::abs(val) > 1.0)
         val /= 10;
   }

   void normalize_ds(dataset& ds) {
      if (is_integral_dataset(ds) && !ds.empty()) {
         object objmin, objmax;
         auto attrcount = ds.cbegin()->size();
         
         objmin.resize(attrcount);
         std::fill(objmin.data.begin(), objmin.data.end(), maxval);

         objmax.resize(attrcount);
         std::fill(objmax.data.begin(), objmax.data.end(), minval);

         for (const auto& obj : ds) {
            for (auto i = 0; i < attrcount; ++i) {
               objmin[i] = std::min(objmin[i], obj[i]);
               objmax[i] = std::max(objmax[i], obj[i]);
            }
         }

         auto objdiff = objmax - objmin;
         for (auto& obj : ds) {
            for (auto i = 0; i < attrcount; ++i) {
               if (objdiff[i])
                  obj[i] = (obj[i] - objmin[i]) / objdiff[i];
            }
         }
      }
   }
}
