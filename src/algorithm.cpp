#include <cmath>
#include <ctime>
#include "clustlib/algorithm.h"
#include "clustlib/utils.h"

namespace clustlib {
   // Neural network common settings
   using settings_nn = neural_network::settings;

   //// Utilities /////////////////////////////////////////
   namespace utils {
      bool contains_negative(const dataset& ds) {
         return std::find_if(ds.cbegin(), ds.cend(), [](const object& obj) {
            return std::find_if(obj.data.cbegin(), obj.data.cend(), [](value_t x) {
               return x < 0;
            }) != obj.data.cend();
         }) != ds.cend();
      }

      value_t kohonen_weight_init_value(value_t coeff, bool cond) {
         constexpr auto bias = 0.5;

         auto generate = []() {
            value_t res = rand();
            normalize_val(res);
            return res;
         };

         value_t res = generate();
         while ((cond && std::abs(res) > coeff) ||
            (!cond && (bias - coeff > res || bias + coeff < res)))
               res = generate();

         return res;
      }

      void kohonen_weights_init(weights& w, std::size_t samples_count, bool cond) {
         srand(time(nullptr));
         value_t coeff = 1 / std::sqrt(samples_count);
         for (auto& wi : w)
            for (auto& wij : wi)
               wij = kohonen_weight_init_value(coeff, cond);
      }

      bool is_correct_settings(const settings_nn& params) {
         return params.eras > 0 && params.learning_rate > 0.0;
      }
   }
   //// Abstract //////////////////////////////////////////
   void algorithm::load_dataset(const clustlib::dataset* ds) noexcept(false) {
      
      __attrcount = 0;
      __samplcount = 0;
      __ds = nullptr;

      if (!ds) return;

      if (!is_integral_dataset(*ds)) {
         throw std::logic_error("Given dataset is not integral. Objects attribute counts doesn't match.");
         return;
      }

      __ds = ds;
      __samplcount = ds->size();
      if (samples_count())
         __attrcount = __ds->cbegin()->size();
   }

   void algorithm::load_metric(metric_f dist) {
      __dist = dist;
   }

   const clustlib::dataset* algorithm::get_dataset() const {
      return __ds;
   }

   std::size_t algorithm::attribute_count() const {
      return __attrcount;
   }

   std::size_t algorithm::samples_count() const {
      return __samplcount;
   }

   clustlib::value_t algorithm::distance(const clustlib::object& x,
      const clustlib::object& y) const noexcept(false) {
      return __dist(x, y);
   }

   metric_f algorithm::metric() const {
      return __dist;
   }
   //// Abstract centroid-based ///////////////////////////
   void centroid_based::result(cluster_vec& clusters) const {
      clusters = __cl;
   }

   void centroid_based::set_cluster_count(std::size_t cluster_count) {
      __clcount = cluster_count;
      __cl.clear();
      __cl.resize(__clcount);
   }

   void centroid_based::move(std::size_t ds_idx, std::size_t cl_idx) {
      auto it = std::find_if(__bridge.cbegin(), __bridge.cend(),
      [ds_idx](const std::pair <std::size_t, std::size_t>& keyval) {
            return keyval.first == ds_idx;
      });
      if (it == __bridge.end()) {
         __bridge[ds_idx] = cl_idx;
         __cl[cl_idx].add(get_dataset()->at(ds_idx));
      }
      else {
         auto old_cl_idx = it->second;
         if (old_cl_idx != cl_idx) {
            __bridge.erase(ds_idx);
            __cl[old_cl_idx].move(get_dataset()->at(ds_idx), __cl[cl_idx]);
            __bridge[ds_idx] = cl_idx;
         }
      }
   }

   const cluster_vec& centroid_based::clusters() const {
      return __cl;
   }

   inline std::size_t centroid_based::clusters_count() const {
      return __clcount;
   }

   void centroid_based::remove_empty() {
      auto it = std::remove_if(__cl.begin(), __cl.end(), [](const cluster& cl) {
         return cl.empty();
      });

      if (it != __cl.end()) {
         __clcount -= std::distance(it, __cl.end());
         __cl.erase(it);
      }
   }

   std::size_t centroid_based::add_cluster() {
      __clcount++;
      __cl.push_back({});
      return __clcount - 1;
   }

   //// Abstract connectivity-based ///////////////////////
   void connectivity_based::result(cluster_tree& clusters) const {
      clusters = __cl;
   }

   //// Common neural network /////////////////////////////
   bool neural_network::clusterize() {
      // reset
      set_cluster_count(clusters_count());
      if (!is_ready())
         return false;
      init_weights();

      auto eras = get_settings().eras;
      auto rate = get_settings().learning_rate;
      auto downturn = std::abs(get_settings().rate_downturn);
      for (auto i = 0; i < eras; ++i) {
         if (rate <= 0 || !loop(rate)) break;
         rate -= downturn;
      }
      
      remove_empty();
      return true;
   }

   void neural_network::load_settings(const settings& params) {
      __s = params;
   }

   void neural_network::load_dataset(const dataset* ds) noexcept(false) {
      __dsnorm = *ds;
      if (__norm) {
         __dsorig = ds;
         normalize_ds(__dsnorm);
      }

      algorithm::load_dataset(&__dsnorm);
   }

   void neural_network::set_normalization(bool b) {
      __norm = b;
   }

   void neural_network::result(cluster_vec& clusters) const {
      centroid_based::result(clusters);
      if (__norm) {
         for (auto& cl : clusters) {
            for (auto& obj : cl.get_dataset()) {
               for (auto i = 0; i < samples_count(); ++i) {
                  if (__dsnorm[i] == obj)
                     obj = __dsorig->at(i);
               }
            }
         }
      }
   }

   const neural_network::settings& neural_network::get_settings() const {
      return __s;
   }

   weights& neural_network::get_weights() {
      return __w;
   }

   const weights& neural_network::get_weights() const {
      return __w;
   }
   
   bool neural_network::is_ready() const {
      const auto& st = get_settings();
      
      bool valid_eras = st.eras > 0;
      bool valid_rate = st.learning_rate > 0 && st.learning_rate <= 1.0;
      bool valid_threshold = st.threshold >= 0;
      
      return valid_eras && valid_rate && valid_threshold;
   }

   std::size_t neural_network::mindist_cluster(const object& obj) const {
      std::size_t minc = -1;
      value_t d_min = maxval;
      const auto& cl = clusters();
      auto& __w = get_weights();

      for (auto i = 0; i < cl.size(); ++i) {
         object centroid(__w[i]);
         value_t dist = distance(obj, centroid);

         if (dist < d_min) {
            d_min = dist;
            minc = i;
         }
      }

      return minc;
   }

   //// Kohonen's unsupervised ////////////////////////////
   kohonen::kohonen(const dataset* ds/*= nullptr*/, bool norm /*= true*/, const settings_nn& params/*= settings_nn()*/,
      std::size_t cluster_count/* = 0*/, metric_f dist/*= &euclidean*/) noexcept(false) {
      set_normalization(norm);
      load_dataset(ds);
      load_settings(params);
      set_cluster_count(cluster_count);
      load_metric(dist);
   }

   bool kohonen::loop(value_t rate) {
      const auto threshold = get_settings().threshold;
      auto& __w = get_weights();
      
      bool changes = true;
      auto samples = samples_count();
      for (auto cur = 0; cur < samples; ++cur) {
         changes = false;
         const object& obj = get_dataset()->at(cur);
         std::size_t minc = mindist_cluster(obj);
         move(cur, minc);

         object learn_obj = obj - object(__w[minc]);
         learn_obj.foreach([rate](value_t& val) {
            val *= rate;
            });

         for (auto i = 0; i < attribute_count(); ++i) {
            if (std::abs(learn_obj[i]) > threshold)
               changes = true;
            __w[minc][i] += learn_obj[i];
         }
      }

      return changes;
   }

   void kohonen::init_weights() {
      weights& w = get_weights();
      w.resize(clusters_count());
      for (auto& wi : w)
         wi.resize(attribute_count());
      utils::kohonen_weights_init(w, samples_count(), utils::contains_negative(*get_dataset()));
   }

   //// Kohonen's self-organized ////////////////////////////
   kohonen_so::kohonen_so(const dataset* ds/* = nullptr*/, bool norm/*= true*/, const settings& params /*= settings()*/,
      value_t crit_d /*= nullval*/, metric_f dist /*= &euclidean*/) noexcept(false) {
      set_normalization(norm);
      set_crit_distance(crit_d);
      load_dataset(ds);
      load_settings(params);
      load_metric(dist);
      set_cluster_count(1);
   }

   void kohonen_so::set_crit_distance(value_t dist) {
      __crit = dist;
   }

   bool kohonen_so::is_ready() const {
      const auto& st = get_settings();

      bool valid_eras = st.eras > 0;
      bool valid_rate = st.learning_rate > 0 && st.learning_rate <= 1.0;
      bool valid_threshold = st.threshold >= 0;
      bool valid_crit = __crit > 0;

      return valid_eras && valid_rate && valid_threshold && valid_crit && samples_count();
   }

   void kohonen_so::init_weights() {
      set_cluster_count(1);
      auto& __w = get_weights();
      __w.resize(1);
      __w[0] = get_dataset()->begin()->data;
   }

   bool kohonen_so::loop(value_t rate) {
      const auto threshold = get_settings().threshold;
      auto& __w = get_weights();

      bool changes = true;
      auto samples = samples_count();
      for (auto cur = 0; cur < samples; ++cur) {
         changes = false;
         const object& obj = get_dataset()->at(cur);
         std::size_t minc = mindist_cluster(obj);
         if (std::abs(distance(obj, __w[minc])) <= __crit)
            move(cur, minc);
         else {
            minc = add_cluster();
            move(cur, minc);
            __w.push_back(obj.data);
         }

         object learn_obj = obj - object(__w[minc]);
         learn_obj.foreach([rate](value_t& val) {
            val *= rate;
         });

         for (auto i = 0; i < attribute_count(); ++i) {
            if (std::abs(learn_obj[i]) > threshold)
               changes = true;
            __w[minc][i] += learn_obj[i];
         }
      }

      return changes;
   }
   //// Kohonen's hierarchical ////////////////////////////
   hierarchical_kohonen::hierarchical_kohonen(const dataset* ds/* = nullptr*/, const settings& params /*= settings()*/,
      value_t crit_d /*= nullval*/, metric_f dist /*= &euclidean*/) noexcept(false) {
      set_crit_distance(crit_d);
      load_dataset(ds);
      load_settings(params);
      load_metric(dist);
   }

   bool hierarchical_kohonen::is_ready() const {
      const auto& st = get_settings();

      bool valid_eras = st.eras > 0;
      bool valid_rate = st.learning_rate > 0 && st.learning_rate <= 1.0;
      bool valid_threshold = st.threshold >= 0;
      bool valid_crit = __crit > 0;

      return valid_eras && valid_rate && valid_threshold && valid_crit && samples_count();
   }

   bool hierarchical_kohonen::clusterize() {
      __cl.parent.clear();
      __cl.children.clear();
      if (!is_ready())
         return false;

      __cl.parent = cluster(*get_dataset());
      if (get_dataset()->size() == 1)
         return true;
      else if (get_dataset()->size() == 2) {
         for (auto i = 0; i < 2; ++i) {
            cluster_tree ct;
            ct.parent = cluster({ get_dataset()->at(i)});
            __cl.children.push_back(ct);
         }
         return true;
      }

      metric_f dist = metric();
      kohonen_so kso(get_dataset(), true, __s, __crit, dist);
      if (!kso())
         return false;
      cluster_vec cv;
      kso.result(cv);

      if (cv.size() == 1 && cv[0].size() == get_dataset()->size())
         return false;

      settings st;
      auto count = cv.size();
      if (count) {
         st.eras = std::max(number_t(get_settings().eras / count), number_t(1));
         st.learning_rate = get_settings().learning_rate / count;
         st.rate_downturn = get_settings().rate_downturn / count;
         st.threshold = get_settings().threshold / count;
         value_t crit = __crit / count;

         for (auto& cl : cv) {
            hierarchical_kohonen hk(&cl.get_dataset(), st, crit, dist);
            if (hk.clusterize()) {
               cluster_tree ct;
               hk.result(ct);
               __cl.children.push_back(ct);
            }
         }
      }

      return true;
   }

   void hierarchical_kohonen::load_settings(const settings& params) {
      __s = params;
   }

   void hierarchical_kohonen::set_crit_distance(value_t dist) {
      __crit = dist;
   }

   const hierarchical_kohonen::settings& hierarchical_kohonen::get_settings() const {
      return __s;
   }
   ////////////////////////////////////////////////////////
}
