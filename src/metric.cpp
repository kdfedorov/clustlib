#include <cmath>
#include "clustlib/metric.h"

namespace clustlib {
   clustlib::value_t fpower_dist(const clustlib::object& x, const clustlib::object& y,
      clustlib::value_t root/*= 2*/, clustlib::value_t power/*= 2*/) noexcept(false) {
      value_t res = nullval;
      if (x.match(y)) {
         auto size = x.size();
         for (auto i = 0; i < size; ++i)
            res += std::pow(x[i] - y[i], power);
         res = std::pow(res, 1 / root);
      }
      return res;
   }

   clustlib::value_t euclidean_sq(const clustlib::object& x,
      const clustlib::object& y) noexcept(false) {
      value_t res = nullval;
      if (x.match(y)) {
         auto size = x.size();
         for (auto i = 0; i < size; ++i)
            res += (x[i] - y[i]) * (x[i] - y[i]);
      }
      return res;
   }

   clustlib::value_t euclidean(const clustlib::object& x,
      const clustlib::object& y) noexcept(false) {
      return std::sqrt(euclidean_sq(x, y));
   }

   clustlib::value_t manhattan(const clustlib::object& x,
      const clustlib::object& y) noexcept(false) {
      value_t res = nullval;
      if (x.match(y)) {
         auto size = x.size();
         for (auto i = 0; i < size; ++i)
            res += std::abs(x[i] - y[i]);
      }
      return res;
   }

   clustlib::value_t chebyshev(const clustlib::object& x,
      const clustlib::object& y) noexcept(false) {
      value_t res = minval;
      if (x.match(y)) {
         auto size = x.size();
         for (auto i = 0; i < size; ++i)
            res = std::max(res, std::abs(x[i] - y[i]));
      }
      return res;
   }
}